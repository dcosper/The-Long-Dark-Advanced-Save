﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDHtool
{
    public class BinLoader
    {
        string FilePath;
        public BinData Data;
        public BinLoader(string filePath)
        {
            FilePath = filePath;
            Data = new BinData();
        }
        public void Load()
        {
            Data.Clear();
            try
            {
                using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    int fileNum = fs.ReadByte();

                    for(int i = 0;i<fileNum;i++)
                    {
                        // the length of the filename
                        byte[] length = new byte[4];
                        fs.Read(length, 0, length.Length);

                        // the filename
                        byte[] fileNameBytes = new byte[BitConverter.ToInt32(length, 0)];
                        fs.Read(fileNameBytes, 0, fileNameBytes.Length);
                        string fileName = Encoding.ASCII.GetString(fileNameBytes);

                        // now the data
                        byte[] dataLength = new byte[4];
                        fs.Read(dataLength, 0, dataLength.Length);

                        byte[] data = new byte[BitConverter.ToInt32(dataLength, 0)];
                        fs.Read(data, 0, data.Length);

                        Data.AddData(fileName, data);
                    }
                }
                System.Windows.Forms.MessageBox.Show("Loaded");
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
    }
}
