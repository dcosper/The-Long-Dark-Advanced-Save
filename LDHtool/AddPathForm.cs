﻿using savewatch;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LDHtool
{
    public partial class AddPathForm : Form
    {
        public AddPathForm()
        {
            InitializeComponent();
        }

        private void btn_Select_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            f.Description = "Choose a savegame directory.";

            DialogResult r = f.ShowDialog();

            if (r == DialogResult.OK)
            {
                comboBoxDirectory.Items.Add(f.SelectedPath);
                comboBoxDirectory.SelectedIndex = 0;
            }
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txt_KeyWord.Text))
            {
                MessageBox.Show("Enter a keyword for this save directory.");
                return;
            }
            if(comboBoxDirectory.Items.Count == 0)
            {
                MessageBox.Show("Select a save directory.");
                return;
            }

            // add it
            if (new XMLManager().AddPath(txt_KeyWord.Text, comboBoxDirectory.Items[0].ToString()))
            {
                MessageBox.Show(string.Format("Keyword {0} added with path {1}", txt_KeyWord.Text, comboBoxDirectory.Items[0].ToString()));
                Form1.This.PopulateOpenMenu();
                this.Close();
            }
            else MessageBox.Show("Error adding path!");
        }

        private void btn_Cancel_Click(object sender, EventArgs e) => this.Close();
        
    }
}
