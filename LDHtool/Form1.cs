﻿using savewatch;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LDHtool
{
    public partial class Form1 : Form
    {
        public static Form1 This;
        public Form1()
        {
            InitializeComponent();
            This = this;
            PopulateOpenMenu();
        }

        public void PopulateOpenMenu()
        {
            ToolStripMenuItem open = menuStrip1.Items[0] as ToolStripMenuItem; // &File

            ToolStripDropDownItem item = null;
            foreach(ToolStripDropDownItem i in open.DropDownItems)
            {
                if(i.Text == "&Open")
                {
                    item = i;
                    break;
                }
            }
            if(item == null)
            {
                MessageBox.Show("Unable to locate menuitem, please notify developer!");
            }
            else
            {
                item.DropDownItems.Clear();
                // populate open
                Dictionary<string, string> data = new XMLManager().GetPathData();

                if(data == null)
                {
                    MessageBox.Show("Unable to load path data, have you run savewatch yet ?!");
                    return;
                }
                foreach(KeyValuePair<string, string> value in data)
                {
                    ToolStripMenuItem d = new ToolStripMenuItem(value.Key);
                    d.Tag = value.Value;
                    d.Click += OpenChildClick;
                    item.DropDownItems.Add(d);
                }
            }
        }

        private void OpenChildClick(object sender, EventArgs e)
        {
            ToolStripMenuItem i = sender as ToolStripMenuItem;

            if (i == null) return;

            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "TLD Bin Save Files|*.bin";
            o.Multiselect = false;
            o.InitialDirectory = i.Tag.ToString();
            DialogResult r = o.ShowDialog();

            if (r == DialogResult.OK)
            {
                BinLoader b = new BinLoader(o.FileName);
                b.Load();
                // add to our list
                listView1.Items.Clear();

                foreach (KeyValuePair<string, byte[]> data in b.Data.FileData)
                {
                    ListViewItem li = new ListViewItem(data.Key.Split('\\').Last());
                    li.Name = data.Key;
                    li.Tag = data.Value;

                    listView1.Items.Add(li);
                }
            }
        }
        private void extractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // try to get the selected item
            // change this to ask if you want to overwrite each item
            if (listView1.Items.Count > 0)
            {
                foreach (ListViewItem i in listView1.Items)
                {
                    DialogResult r = MessageBox.Show(string.Format("Overwrite file {0} ?", i.Name), "Overwrite File ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (r == DialogResult.OK)
                    {
                        using (FileStream fs = new FileStream(i.Name, FileMode.Create, FileAccess.Write))
                        {
                            byte[] file = i.Tag as byte[];
                            fs.Write(file, 0, file.Length);
                        }
                    }
                }
            }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPathForm a = new AddPathForm();
            a.ShowDialog();
        }
    }
}
