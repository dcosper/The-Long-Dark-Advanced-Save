﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDHtool
{
    public class BinData
    {
        public Dictionary<string, byte[]> FileData;

        public BinData()
        {
            FileData = new Dictionary<string, byte[]>();
        } 
           
        public void AddData(string filePath, byte[] data)
        {
            FileData.Add(filePath, data);
        }
        public void Clear()
        {
            FileData.Clear();
        }
    }
}
