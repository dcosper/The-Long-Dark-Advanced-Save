﻿using System;
using System.IO;
using System.Reflection;

namespace savewatch
{
    public class Globals
    {
        public static string LDAppPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Hinterland\\TheLongDark");
        public static string XMLData = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "GameList.xml");
    }
}
