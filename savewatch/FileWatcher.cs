﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace savewatch
{
    public class FileWatcher : IDisposable
    {
        public FileSystemWatcher watcher;
        public Timer watchTimer;
        private List<string> changes;
        private string watchedpath;
        public FileWatcher(string path)
        {
            watchedpath = path;
            watcher = new FileSystemWatcher();
            watcher.Filter = "*";
            watcher.Path = path;
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;

            changes = new List<string>();

            watchTimer = new Timer(WorkOnCurentChanges);
            watchTimer.Change(0, 10000); // every 10 seconds

            Console.WriteLine("Watching path {0} type exit to stop.", path);
        }

        private void WorkOnCurentChanges(object state)
        {
            if (changes.Count > 0)
            {
                string fileName = string.Format("savechange_{0:G}.bin", DateTime.Now).Replace(' ', '_').Replace('/', '_').Replace(':', '_');

                try
                {
                    using (FileStream fs = new FileStream(Path.Combine(watchedpath, fileName), FileMode.Create, FileAccess.Write))
                    {
                        byte totalChanges = (byte)changes.Count;
                        fs.WriteByte(totalChanges);
                        foreach (string file in changes)
                        {
                            byte[] data = File.ReadAllBytes(file);
                            byte[] path = Encoding.ASCII.GetBytes(file);

                            byte[] pathLength = BitConverter.GetBytes(path.Length); // int
                            byte[] dataLength = BitConverter.GetBytes(data.Length);

                            fs.Write(pathLength, 0, pathLength.Length);

                            fs.Write(path, 0, path.Length);

                            fs.Write(dataLength, 0, dataLength.Length);

                            fs.Write(data, 0, data.Length);
                            Console.WriteLine("Added {0}", file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return;
                }
                changes.Clear();
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {

            if (!e.FullPath.EndsWith("temp") && !e.FullPath.EndsWith("bin") && !changes.Contains(e.FullPath))
            {
                changes.Add(e.FullPath);
            }
        }

        public void Dispose()
        {
            watchTimer.Change(0, Timeout.Infinite);
            watcher.Changed -= new FileSystemEventHandler(OnChanged);

            watcher.Dispose();
            watchTimer.Dispose();
        }
        //async Task<bool> 
    }
}
