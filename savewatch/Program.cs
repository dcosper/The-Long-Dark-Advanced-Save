﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace savewatch
{
    class Program
    {
        static ConsoleCommands cmds;
        static void Main(string[] args)
        {
            if (!File.Exists(Globals.XMLData))
                new XMLManager().CreateGameList();
            cmds = new ConsoleCommands();
            cmds.Start();
        }
    }
}
