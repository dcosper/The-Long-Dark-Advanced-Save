﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace savewatch
{
    public class XMLManager
    {
        private XDocument doc;
        public XMLManager()
        {

        }
        public bool CreateGameList()
        {
            doc = new XDocument(
                new XElement("ROOT",
                new XComment("Known list of save directories for games."),
                new XComment("keyword ----------- path"),
                new XElement("GameList")));
            return TrySaveDoc();
        }
        public Dictionary<string, string> GetPathData()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (!TryLoadDoc())
                return null;

            XElement gameList = doc.Element("ROOT").Element("GameList");

            foreach (XElement n in gameList.Descendants())
                result.Add(n.Name.ToString(), n.Value);
            return result;
        }
        private bool TryLoadDoc()
        {
            try
            {
                doc = XDocument.Load(Globals.XMLData);
                return true;
            }
            catch { return false; }
        }
        private bool TrySaveDoc()
        {
            try
            {
                doc.Save(Globals.XMLData);
                return true;
            }
            catch { return false; }
        }
        public bool AddPath(string keyword, string directory)
        {
            if (!TryLoadDoc())
                return false;

            if (!Directory.Exists(directory))
                throw new DirectoryNotFoundException(directory);

            XElement gameList = doc.Element("ROOT").Element("GameList");

            XElement newGame = new XElement(keyword, directory);

            gameList.Add(newGame);
            return TrySaveDoc();
        }
    }
}
