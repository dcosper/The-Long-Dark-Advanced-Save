﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace savewatch
{
    public class ConsoleCommands
    {
        public bool IsListening = true;
        private List<FileWatcher> watchers;
        public void Start()
        {
            IsListening = true;
            Listen();
        }
        public void Stop() => IsListening = false;

        public void Listen()
        {
            watchers = new List<FileWatcher>();
            while(IsListening)
            {
                Console.Write(":> ");

                string line = Console.ReadLine();

                string[] args = line.Split(' ');

                switch(args[0])
                {
                    case "gamelist":
                        {
                            Dictionary<string, string> v = new XMLManager().GetPathData();

                            if (v == null)
                                Console.WriteLine("null");

                            foreach (KeyValuePair<string, string> b in v)
                                Console.WriteLine("{0} -> {1}", b.Key, b.Value);
                        }
                        break;
                    case "watch":
                        {
                            // we need a keywork
                            if (args.Length < 2)
                            {
                                Console.WriteLine("Which save game do you wish to watch ? please use a key EX: watch tld");
                            }
                            else
                            {
                                Dictionary<string, string> list = new XMLManager().GetPathData();

                                FileWatcher w = null;
                                foreach(KeyValuePair<string, string> value in list)
                                {
                                    if(value.Key.ToLower() == args[1])
                                    {
                                        Task.Run(() =>
                                        {
                                            w = new FileWatcher(value.Value);
                                            if (w == null)
                                                Console.WriteLine("Unable to locate save game by keyword {0}!", args[1]);
                                            watchers.Add(w);
                                        });
                                    }
                                }
                                
                            }
                        }
                        break;
                    case "exit":
                        {
                            foreach (FileWatcher f in watchers)
                                f.Dispose();
                            IsListening = false;
                        }
                        break;
                }
            }
        }
    }
}
