This project contains 2 files to be run
savewatch watches the save directory, currently you use it only for TLD
but it is written so later i can add other save directories, to begin
compile the projects in visual studio, then run 

savewatch tld

this will start the watcher, when it detects a change in your save files
it will archive that change into a bin file with the date and time it did so
you can then run the windows form LDHTool and open those bin files and then
extract the save file for your game back into the save directory, overwriting 
the one you just fubared.

The normal TLD save dir is 
C:\users\yourname\AppData\Local\Hinterlands\TheLongDark

if this save directory is diferent than yours, then this will not 
work for you.

I plan on adding a XML config document that allows you to add
a path to a save directory and a key word so you can watch custom directories

I currently use this for The Long Dark and it works fine